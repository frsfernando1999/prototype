using System;
using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Components")] [SerializeField]
    private Rigidbody2D rigidBody;

    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer spriteRenderer;

    [Header("AI Control")] [SerializeField]
    private Transform patrolPoints;

    [SerializeField] private float patrolPointTolerance = 0.1f;
    [SerializeField] private float patrolPointWaitTime = 2.0f;
    [SerializeField] private float maxSpeed = 2.0f;
    [SerializeField] private float speed = 1.0f;
    
    [Header("Health")] 
    [SerializeField] private float maxHealth = 3.0f;
    [SerializeField] private float hurtTime = 0.5f;
    private float _health = 1.0f;

    private AIState _currentState = AIState.Idling;
    private Vector3 _destination;
    private float _waitTime = 0f;
    private float _moveDirection;

    private static readonly int Speed = Animator.StringToHash("Speed");

    private enum AIState
    {
        Idling,
        Moving,
        Hurting,
    }

    private void Start()
    {
        _destination = patrolPoints.GetChild(0).position;
        _health = maxHealth;
    }

    private void Update()
    {
        if (_currentState == AIState.Idling)
        {
            IdleState();
        }
        if (_currentState == AIState.Moving)
        {
            if (patrolPoints == null) return;
            MoveState();
        }
        if (_currentState == AIState.Hurting)
        {
            HurtingState();
        }
        animator.SetFloat(Speed, rigidBody.velocity.x);
    }
    
    private void HurtingState()
    {
        _moveDirection = 0f;
        rigidBody.velocity = Vector2.zero;
    }

    private void MoveState()
    {
        var destination = Vector3.Distance(transform.position, _destination);
        if (destination < patrolPointTolerance)
        {
            _currentState = AIState.Idling;
            _destination = GetNewDestination();
            _moveDirection = 0f;
            rigidBody.velocity = new Vector2(0.0f, 0.0f);
        }
        else
        {
            if (_destination.x - transform.position.x > 0.1)
            {
                spriteRenderer.flipX = false;
                _moveDirection = 1f;
            }
            else if (_destination.x - transform.position.x < -0.1)
            {
                spriteRenderer.flipX = true;
                _moveDirection = -1f;
            }
        }
    }

    private void FixedUpdate()
    {
        if (rigidBody.velocity.magnitude < maxSpeed)
        {
            rigidBody.velocity += new Vector2(_moveDirection * speed, 0f);
        }
    }

    private Vector3 GetNewDestination()
    {
        for (int i = 0; i < patrolPoints.childCount; i++)
        {
            if (patrolPoints.GetChild(i).position == _destination && i + 1 < patrolPoints.childCount)
            {
                return patrolPoints.GetChild(i + 1).position;
            }
        }

        return patrolPoints.GetChild(0).position;
    }

    private void IdleState()
    {
        _waitTime += Time.deltaTime;
        if (_waitTime > patrolPointWaitTime)
        {
            _currentState = AIState.Moving;
            _waitTime = 0.0f;
        }
    }

    public void TakeDamage(int damage)
    {
        _currentState = AIState.Hurting;
        StartCoroutine(HurtReset());
        _health -= damage;
        if (_health <= 0)
        {
            Die();
        }
    }

    private IEnumerator HurtReset()
    {
        yield return new WaitForSeconds(hurtTime);
        _currentState = AIState.Moving;
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}