using System;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Vector2 _movePos;
    private Vector2 _startPos;
    
    [SerializeField] private bool vertical; 
    [SerializeField] private bool horizontal;

    [SerializeField] [Range(0, 4)] private float moveSpeed;
    [SerializeField] [Range(0, 2)] private float moveDistance;

    private void Start()
    {
        _startPos = transform.position;
    }

    void Update()
    {
        if (vertical)
        {
            _movePos.y = _startPos.y + Mathf.Sin(Time.time * moveSpeed) * moveDistance;
            transform.position = new Vector2(transform.position.x, _movePos.y);
        }

        if (horizontal)
        {
            _movePos.x = _startPos.x + Mathf.Sin(Time.time * moveSpeed) * moveDistance;
            transform.position = new Vector2(_movePos.x, transform.position.y);

        }
    }
}
