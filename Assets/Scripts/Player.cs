using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    public event EventHandler OnGameReset;
    public event EventHandler OnDie;
    public event EventHandler OnWin;
    public event EventHandler<OnHealthChangedEventArgs> OnHealthChanged;
    public event EventHandler<OnStaminaChangedEventArgs> OnStaminaChanged;


    
    [Header("Component Dependencies")]
    [SerializeField] private AudioSource jumpSound;
    [SerializeField] private AudioSource hurtSound;
    [SerializeField] private AudioSource attackSound;
    [SerializeField] private Control control;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform weaponCollider;
    
    [Header("Stamina")]
    [SerializeField] private float maxStamina = 100.0f;
    [SerializeField] private float staminaRegenRate = 5.0f;

    [Header("Health Related Variables")]
    [SerializeField] private int maxHealth = 10;
    [SerializeField] private float invincibleTime = 0.2f;
    [SerializeField] private float deathTime = 1.0f;

    [Header("Damage")] [SerializeField] private int baseDamage = 1;
    
    private bool _invincible;
    private bool _isDead;
    private int _health;
    private Vector2 _initialPos;
    private float _stamina;
    
    private static readonly int RollAnim = Animator.StringToHash("RollAnim");
    private static readonly int Hurt = Animator.StringToHash("Hurt");
    private static readonly int IsDead = Animator.StringToHash("IsDead");
    private static readonly int Reset = Animator.StringToHash("Reset");
    
    public float GetStamina() => _stamina;
    public float GetHealth() => _health;
    public float GetMaxHealth() => maxHealth;

    private void Start()
    {
        _initialPos = transform.position;
        _health = maxHealth;
        _stamina = maxStamina;
        NotifyHealth();
    }
    private void Update()
    {   
        RegenerateStamina();
    }

    private void RegenerateStamina()
    {
        _stamina = Mathf.Clamp(_stamina + staminaRegenRate * Time.deltaTime, 0, maxStamina);
        NotifyStamina();
    }

    private void Die()
    {
        animator.SetTrigger(IsDead);
        _isDead = true;
        control.enabled = false;
        StartCoroutine(DeathTimer());
    }

    private IEnumerator DeathTimer()
    {
        yield return new WaitForSeconds(deathTime);
        OnDie?.Invoke(this, EventArgs.Empty);
        Time.timeScale = 0.0f;
    }


    public void Win()
    {
        GetComponent<Control>().enabled = false;
        OnWin?.Invoke(this, EventArgs.Empty);
        Time.timeScale = 0.0f;
    }

    public void JumpLogic()
    {
        jumpSound.Play();
    }

    public void ResetGame()
    {
        Time.timeScale = 1.0f;
        control.enabled = true;
        transform.position = _initialPos;
        control.ResetSpeed();
        if (_health <= 0) animator.SetTrigger(Reset);
        _health = maxHealth;
        _stamina = maxStamina;
        _isDead = false;
        NotifyHealth();
        NotifyStamina();
        NotifyTime();
    }

    private void NotifyTime()
    {
        OnGameReset?.Invoke(this, EventArgs.Empty);
    }

    public void Roll(float staminaCost)
    {
        _stamina -= staminaCost;
        animator.SetTrigger(RollAnim);
        NotifyStamina();
    }

    public void TakeDamage(int damageAmount)
    {
        if (!_invincible) // Não implementar em primeiro momento
        {
            _health = Mathf.Clamp(_health - damageAmount, 0, maxHealth);
            NotifyHealth();
            _invincible = true;
            hurtSound.Play();
            StartCoroutine(ResetInvincible());
            if (_health  <= 0)
            {
                Die();
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                animator.SetTrigger(Hurt);
            }
        }
    }

    private IEnumerator ResetInvincible()
    {
        yield return new WaitForSeconds(invincibleTime);
        _invincible = false;
    }
    private void NotifyStamina()
    {
        OnStaminaChanged?.Invoke(this, new OnStaminaChangedEventArgs
        {
            Stamina = _stamina,
            MaxStamina = maxStamina
        });
    }

    private void NotifyHealth()
    {
        OnHealthChanged?.Invoke(this, new OnHealthChangedEventArgs
        {
            Health = _health,
            MaxHealth = maxHealth
        });
    }

    public void Attack(int comboID)
    {
        weaponCollider.gameObject.SetActive(true);
        StartCoroutine(ResetCollider());
        PlayAttackSound();
    }

    private void PlayAttackSound()
    {
        attackSound.Play();
    }

    private IEnumerator ResetCollider()
    {
        yield return new WaitForSeconds(0.2f);
        weaponCollider.gameObject.SetActive(false);
    }

    public int GetBaseDamage() => baseDamage;

    public void RestoreHealth(int healthToRestore)
    {
        _health = Mathf.Clamp(_health + healthToRestore, 0, maxHealth);
        NotifyHealth();
    }

    public bool GetIsDead()
    {
        return _isDead;
    }
}
public class OnStaminaChangedEventArgs : EventArgs
{
    public float MaxStamina;
    public float Stamina;
}
public class OnHealthChangedEventArgs : EventArgs
{
    public int MaxHealth;
    public int Health;
}