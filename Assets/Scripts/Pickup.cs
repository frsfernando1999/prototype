using UnityEngine;

public class Pickup : MonoBehaviour
{
    [SerializeField] private int healthToRestore;
    private void OnTriggerEnter2D(Collider2D other)
    {
        Player player = other.GetComponent<Player>();
        if (player != null && player.GetHealth() > 0 && player.GetHealth() < player.GetMaxHealth())
        {
            player.RestoreHealth(healthToRestore);
            Destroy(gameObject);
        }
    }
}
