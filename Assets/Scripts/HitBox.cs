using UnityEngine;
public class HitBox : MonoBehaviour
{

    [SerializeField] private int damage = 1;
    private void OnTriggerEnter2D(Collider2D other)
    {
        Player player= other.GetComponent<Player>();
        if (player)
        {
            player.TakeDamage(damage);
        }
    }
}
