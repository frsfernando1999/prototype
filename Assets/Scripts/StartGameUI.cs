using UnityEngine;
using UnityEngine.UI;

public class StartGameUI : MonoBehaviour
{
    [SerializeField] private Button startGameBtn;
    [SerializeField] private Button quitGameBtn;

    private void OnEnable()
    {
        startGameBtn.onClick.AddListener(StartGame);
        quitGameBtn.onClick.AddListener(QuitGame);
    }

    private void QuitGame()
    {
        Application.Quit();
    }

    private void StartGame()
    {
        Loader.Load(Loader.Scene.Part2);
    }
}
