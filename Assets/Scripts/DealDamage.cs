using Unity.Mathematics;
using UnityEngine;

public class DealDamage : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private ParticleSystem particles;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy)
        {
            ParticleSystem particlesInstance = Instantiate(particles, other.transform.position, quaternion.identity);
            Destroy(particlesInstance, particlesInstance.main.startLifetime.constantMax);
            enemy.TakeDamage(player.GetBaseDamage());
        }
    }
}
