using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class Control : MonoBehaviour
{
    private static int _noOfClicks;


    [Header("Component Dependencies")] [SerializeField]
    private Rigidbody2D rb;

    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Animator animator;
    [SerializeField] private Player player;
    [SerializeField] private Transform weaponCollider;

    [Header("Movement Speed")] [SerializeField]
    private float moveSpeed = 2.0f;

    [Header("Rolling")] [SerializeField] private float rollStaminaCost = 20.0f;
    [SerializeField] private float rollCooldown = 1.5f;
    [SerializeField] private float rollSpeed = 10.0f;

    [Header("Jumping")] [SerializeField] private float jumpPower = 12.0f;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private LayerMask platformLayer;

    [Header("Combo")] [SerializeField] private int maxComboLength = 3;
    [SerializeField] private float maxComboDelay = 0.5f;
    private float _lastClickedTime;


    private bool _canRoll = true;
    [SerializeField] private PlayerState _currentState;

    private bool _isGrounded;
    private Vector2 _moveInput;

    private static readonly int Attack1 = Animator.StringToHash("Attack1");
    private static readonly int Attack2 = Animator.StringToHash("Attack2");
    private static readonly int Attack3 = Animator.StringToHash("Attack3");
    private static readonly int Speed = Animator.StringToHash("Speed");
    private static readonly int Grounded = Animator.StringToHash("Grounded");
    private static readonly int AttackRes = Animator.StringToHash("ResetAttack");


    private void Update()
    {
        _isGrounded = LayerCheck(groundLayer) || LayerCheck(platformLayer);
        Collider2D platform = LayerCheck(platformLayer);
        if (platform)
        {
            transform.SetParent(platform.transform);
        }
        else
        {
            transform.SetParent(null);
        }

        if (_currentState != PlayerState.Rolling && _currentState != PlayerState.Attacking)
        {
            _currentState = _isGrounded ? PlayerState.Unoccupied : PlayerState.InAir;
        }

        animator.SetFloat(Speed, _moveInput.x);
        animator.SetBool(Grounded, _isGrounded);

        ResetAttack();
        Run();
        FlipSprite();
    }

    private Collider2D LayerCheck(LayerMask layerMask)
    {
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.5f, 0.25f), CapsuleDirection2D.Horizontal,
            0.0f, layerMask);
    }

    private void ResetAttack()
    {
        bool isAttackAnimation =
            animator.GetCurrentAnimatorStateInfo(0).IsName("Hit1") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("Hit2") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("Hit3");
        if (Time.time - _lastClickedTime > maxComboDelay && _noOfClicks > 0)
        {
            _currentState = PlayerState.Unoccupied;
            _noOfClicks = 0;
            if (isAttackAnimation) animator.SetTrigger(AttackRes);
        }
    }

    private void FlipSprite()
    {
        if (_moveInput.x > 0.1f)
        {
            spriteRenderer.flipX = false;
            Quaternion rotation = weaponCollider.transform.rotation;
            weaponCollider.transform.rotation = new Quaternion(rotation.x, 0.0f, rotation.z, rotation.w);
        }
        else if (_moveInput.x < -0.1f)
        {
            spriteRenderer.flipX = true;
            Quaternion rotation = weaponCollider.transform.rotation;
            weaponCollider.transform.rotation = new Quaternion(rotation.x, 180.0f, rotation.z, rotation.w);
        }
    }

    private void Run()
    {
        rb.gravityScale = _currentState is PlayerState.InAir or PlayerState.Rolling ? 3 : 0;

        if (_moveInput.x != 0 && _currentState is PlayerState.Unoccupied or PlayerState.InAir)
        {
            transform.position += new Vector3(_moveInput.x * moveSpeed * Time.deltaTime, 0, 0);
        }
    }

    private void OnJump(InputValue value)
    {
        if (_currentState == PlayerState.Unoccupied)
        {
            rb.velocity = new Vector2(0, jumpPower);
            player.JumpLogic();
        }
    }

    private void OnMove(InputValue value)
    {
        _moveInput = value.Get<Vector2>();
    }

    private void OnRoll(InputValue value)
    {
        if (player.GetStamina() > rollStaminaCost && _currentState == PlayerState.Unoccupied && _canRoll)
        {
            _canRoll = false;
            _currentState = PlayerState.Rolling;
            player.Roll(rollStaminaCost);
            StartCoroutine(ResetRoll());
            rb.velocity = spriteRenderer.flipX ? new Vector2(-rollSpeed, 0f) : new Vector2(rollSpeed, 0f);
        }
    }

    private void OnAttack()
    {
        if (_currentState != PlayerState.Unoccupied &&
            _currentState != PlayerState.Attacking) return;
        if (player.GetIsDead() || _currentState == PlayerState.InAir) return;
        _lastClickedTime = Time.time;
        _noOfClicks++;
        _currentState = PlayerState.Attacking;

        if (_noOfClicks == 1)
        {
            animator.SetTrigger(Attack1);
            player.Attack(1);
        }

        _noOfClicks = Mathf.Clamp(_noOfClicks, 0, maxComboLength);

        if (_noOfClicks >= 2 && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.7f &&
            animator.GetCurrentAnimatorStateInfo(0).IsName("Hit1"))
        {
            animator.SetTrigger(Attack2);
            player.Attack(2);
        }

        if (_noOfClicks >= 3 && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.7f &&
            animator.GetCurrentAnimatorStateInfo(0).IsName("Hit2"))
        {
            animator.SetTrigger(Attack3);
            player.Attack(3);
        }
    }

    private void ResetVelocityAnim()
    {
        _currentState = PlayerState.Unoccupied;
        rb.velocity = Vector2.zero;
    }

    private IEnumerator ResetRoll()
    {
        yield return new WaitForSeconds(rollCooldown);
        _canRoll = true;
    }

    public Vector2 GetMoveInput()
    {
        return _moveInput;
    }

    public void ResetSpeed()
    {
        rb.velocity = Vector2.zero;
    }

    private enum PlayerState
    {
        Unoccupied,
        InAir,
        Rolling,
        Attacking
    }
}