using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WinLossScreenUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Button button;

    private Player _player;

    private void Awake()
    {
        _player = GameObject.FindWithTag("Player").GetComponent<Player>();
    }

    private void OnEnable()
    {
        _player.OnWin += OnWin_ShowUI;
        _player.OnDie += OnDie_ShowUI;
        button.onClick.AddListener(Reset);
    }
    
    private void OnWin_ShowUI(object sender, EventArgs e)
    {
        SetText("You Won!");
        Show();
    }

    private void OnDie_ShowUI(object sender, EventArgs e)
    {
        SetText("You Died");
        Show();
    }


    void Start()
    {
        Hide();
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void SetText(string newText)
    {
        text.text = newText;
    }

    private void Reset()
    {
        Hide();
        _player.ResetGame();
    }
}
