using System;
using System.Collections;
using UnityEngine;

public class Passthrough : MonoBehaviour
{
    private Control _player;
    
    private Collider2D _collider2D;
    private bool _playerOnPlatform; 
    

    private void Awake()
    {
        _collider2D = GetComponent<Collider2D>();
        _player = GameObject.FindWithTag("Player").GetComponent<Control>();
    }

    private void Update()
    {
        if (_player && _player.GetMoveInput().y < 0)
        {
            _collider2D.enabled = false;
            StartCoroutine(EnableCollider());
        }
    }

    private IEnumerator EnableCollider()
    {
        yield return new WaitForSeconds(0.5f);
        _collider2D.enabled = true;
    }

    private void SetPlayerOnPlatform(Collision2D other, bool value)
    {
        var player = other.gameObject.GetComponent<Control>();
        if (player)
        {
            _playerOnPlatform = value;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        SetPlayerOnPlatform(other, true );
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        SetPlayerOnPlatform(other, true );
    }
}
