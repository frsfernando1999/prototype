using System;
using UnityEngine;
using UnityEngine.Serialization;

public class HealthAndStaminaUI : MonoBehaviour
{
    [SerializeField] private RectTransform staminaBar;
    [SerializeField] private RectTransform healthTransform;
    [SerializeField] private GameObject healthIcon;
    
    private Player _player;
    private void Awake()
    {
        _player = GameObject.FindWithTag("Player").GetComponent<Player>();
    }


    private void OnEnable()
    {
        _player.OnStaminaChanged += OnStaminaChanged_UpdateStaminaBar;
        _player.OnHealthChanged += OnHealthChanged_UpdateHealth;
    }

    private void OnHealthChanged_UpdateHealth(object sender, OnHealthChangedEventArgs healthArgs)
    {
        DestroyIcons();
        GameObject[] healthIcons = CreateHealthIcons(healthArgs.MaxHealth);
        SetLostHealth(healthIcons, healthArgs.Health, healthArgs.MaxHealth);
    }
    
    private void DestroyIcons()
    {
        foreach (Transform child in healthTransform)
        {
            Destroy(child.gameObject);
        }
    }
    private GameObject[] CreateHealthIcons(int healthArgsMaxHealth)
    {
        GameObject[] healthIcons = new GameObject[healthArgsMaxHealth];
        for (int i = 0; i < healthArgsMaxHealth; i++)
        {
            healthIcons[i] = Instantiate(healthIcon, healthTransform);
        }
        return healthIcons;
    }
    private void SetLostHealth(GameObject[] healthIcons, int health, int maxHealth)
    {
        for (int i = health; i < maxHealth; i++)
        {
            healthIcons[i].transform.GetChild(0).gameObject.SetActive(false);
            healthIcons[i].transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private void OnStaminaChanged_UpdateStaminaBar(object sender, OnStaminaChangedEventArgs staminaArgs)
    {
        staminaBar.transform.localScale = new Vector3(staminaArgs.Stamina/staminaArgs.MaxStamina,
                                                        staminaBar.transform.localScale.y,
                                                        staminaBar.transform.localScale.z);
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
