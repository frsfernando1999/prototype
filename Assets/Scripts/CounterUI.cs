using System;
using TMPro;
using UnityEngine;

public class Counter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI counterText;

    private float _seconds;
    private int _minutes;

    private void Start()
    {
        Player player = GameObject.FindWithTag("Player").GetComponent<Player>();
        player.OnGameReset += ResetTime;
    }

    void Update()
    {
        _seconds += Time.deltaTime;
        if (_seconds >= 59.9f)
        {
            _seconds = 0.0f;
            _minutes++;
        }

        if (_seconds < 10)
        {
            counterText.text = "" + _minutes + ":0" + Mathf.FloorToInt(_seconds);
        }
        if (_minutes < 10)
        {
            counterText.text = "0" + _minutes + ":" + Mathf.FloorToInt(_seconds);
        }
        if (_minutes < 10 && _seconds < 10)
        {
            counterText.text = "0" + _minutes + ":0" + Mathf.FloorToInt(_seconds);
        }

        if (_minutes > 10 && _seconds > 10)
        {
            counterText.text = "" + _minutes + ":" + Mathf.FloorToInt(_seconds);
        }
    }

    private void ResetTime(object sender, EventArgs eventArgs)
    {
        _seconds = 0.0f;
        _minutes = 0;
    }
}