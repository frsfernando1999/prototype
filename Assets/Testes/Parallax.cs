using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float _startPos; 
    [SerializeField] private GameObject cam;
    [SerializeField] private float parallaxEffect;
    
    private void Start()
    {
        _startPos = transform.position.x;
    }

    private void Update()
    {
        float distance = cam.transform.position.x * parallaxEffect;
        transform.position = new Vector3(_startPos + distance, transform.position.y, transform.position.z);
    }
}
